import axios from "axios";
const api = () => {
  let api = axios.create({
    baseURL: process.env.API_URL,
    timeout: 20000,
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
  });
  return api;
};

export default api;
