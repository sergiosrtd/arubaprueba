const SHOW =  ({IF,children})=>{
    return IF ? children : <></>
}

export default SHOW