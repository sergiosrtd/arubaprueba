module.exports = {
  reactStrictMode: true,
  env: {
    api_url: 'https://www.getaruba.com',
  },
  images: {
    loader: "imgix",
    path: "https://noop/",
},
}
