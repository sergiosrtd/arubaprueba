import { useState } from "react"
import EyeIcon from "../../components/icons/Eye"
import EyeCoveredIcon from "../../components/icons/EyeCovered"
import SHOW from "../../components/SHOW"
import Link from "next/link"
const Login = ()=>{
    const [email,setEmail] = useState("")
    const [password,setPassword] = useState("")
    const [showPassword,setShowPassword] = useState(false)
    return <div className="container max-w-full mx-auto py-24 px-6">
                <div className="font-sans">
                    <div className="max-w-sm mx-auto px-6">
                        <div className="relative flex flex-wrap">
                            <div className="w-full relative">
                                <div className="mt-6">
                                    <div className="mb-5 pb-1border-b-2 text-center font-base text-gray-700"></div>
                                    <div className="text-center font-semibold text-black">
                                        ARUBA
                                    </div>
                                    <form className="mt-8">
                                        <div className="mx-auto max-w-lg">
                                            <div className="py-2">
                                                <span className="px-1 text-sm text-gray-600">Correo</span>
                                                <input  placeholder=""
                                                        required
                                                        onChange={setEmail} 
                                                        type="email"
                                                        className="text-md block px-3 py-2  rounded-lg w-full bg-white border-2 border-gray-300 placeholder-gray-600 shadow-md focus:placeholder-gray-500 focus:bg-white focus:border-gray-600 focus:outline-none"/>
                                            </div>
                                            <div className="py-2">
                                                <span className="px-1 text-sm text-gray-600">Contraseña</span>
                                                <div className="relative">
                                                    <input  placeholder=""
                                                            required
                                                            onChange={setPassword} 
                                                            className="text-md block px-3 py-2 rounded-lg w-full bg-white border-2 border-gray-300 placeholder-gray-600 shadow-md
                                                                                focus:placeholder-gray-500
                                                                                focus:bg-white 
                                                                                focus:border-gray-600  
                                                                                focus:outline-none"
                                                            type={showPassword ? 'text' : 'password'}
                                                    />
                                                    <div onClick={()=>setShowPassword(!showPassword)} className="absolute inset-y-0 right-0 pr-3 flex items-center text-sm leading-5">
                                                        <SHOW IF={showPassword}>
                                                            <EyeCoveredIcon/>
                                                        </SHOW>
                                                        <SHOW IF={!showPassword}>
                                                            <EyeIcon/>
                                                        </SHOW>
                                                    </div>
                                                </div>
                                            </div>
                                            <button className="mt-3 text-lg font-semibold 
                                                        bg-gray-800 w-full text-white rounded-lg
                                                        px-6 py-3 block shadow-xl hover:text-white hover:bg-black">
                                                INGRESAR
                                            </button>
                                            <div className="text-center font-semibold text-gray-600 my-5">
                                                O ingresar con
                                            </div>
                                            <button className="mt-3  text-lg font-semibold 
                                                        bg-blue-600 w-full text-white rounded-lg
                                                        px-6 py-3 block shadow-xl hover:text-white hover:bg-blue-500">
                                                FACEBOOK
                                            </button>
                                            <button className="mt-3 text-lg font-semibold 
                                                        bg-red-600 w-full text-white rounded-lg
                                                        px-6 py-3 block shadow-xl hover:text-white hover:bg-red-500">
                                                GOOGLE
                                            </button>
                                        </div>
                                    </form>
                                    <div className="text-center font-semibold text-gray-600 my-5">
                                        ¿No tiene una cuenta?
                                    </div>
                                    <Link href="/register">
                                        <a className="mt-3 text-lg text-center font-semibold text-gray-600  w-full
                                                        px-6 py-3 block">
                                                REGISTRARSE
                                        </a>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
}

export default Login

